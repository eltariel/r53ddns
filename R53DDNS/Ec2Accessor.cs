﻿using System;
using System.Collections.Generic;
using System.Linq;
using Amazon;
using Amazon.EC2;
using Amazon.EC2.Model;
using Amazon.Runtime;
using NLog;

namespace R53DDNS
{
    public class Ec2Accessor
    {
        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        private readonly SystemConfig config;
        private readonly AWSCredentials credentials;
        private readonly IEnumerable<RegionEndpoint> regions;

        public Ec2Accessor(SystemConfig config)
        {
            this.config = config;
            this.credentials = new BasicAWSCredentials(config.Ec2.AccessKey, config.Ec2.SecretKey);
            this.regions = config.Ec2.Regions.Select(r => RegionEndpoint.GetBySystemName(r));
        }

        public List<DnsMapping> GetDnsMappings()
        {
            var mappings = new List<DnsMapping>();
            foreach (var region in regions)
            {
                log.Debug($"Instances in region {region}:");
                var client = new AmazonEC2Client(credentials, region);

                foreach (var instance in GetInstances(client).Where(it => it.State.Name == InstanceStateName.Running))
                {
                    var m = GetDnsForInstance(instance);
                    if (m != null)
                    {
                        mappings.Add(m);
                    }
                }
            }

            return mappings;
        }

        private static IEnumerable<Instance> GetInstances(IAmazonEC2 client)
        {
            var describeInstancesResponse = client.DescribeInstances();
            return describeInstancesResponse.Reservations.SelectMany(r => r.Instances);
        }

        private DnsMapping GetDnsForInstance(Instance instance)
        {
            DnsMapping dnsMapping = null;

            var dt = instance.Tags.FirstOrDefault(t => t.Key == config.Ec2.DnsTagName);
            if (dt != null)
            {
                dnsMapping = new DnsMapping(dt.Value, instance.PublicIpAddress);
            }

            var instanceName = instance.Tags.First(t => t.Key == "Name").Value;
            log.Debug($"Instance [{instanceName}] @ {instance.PublicIpAddress} - DNS name [{dt?.Value ?? "<not set>"}]");

            return dnsMapping;
        }
    }
}