using System;
using System.Collections.Generic;
using Amazon;
using Amazon.Route53;
using Amazon.Runtime;

namespace R53DDNS
{
    public class SystemConfig
    {
        public Route53Config Route53 { get; set; }
        public Ec2Config Ec2 { get; set; }
        public TimeSpan UpdateInterval { get; set; }
    }

    public class Route53Config
    {
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }
        public string DnsZoneName { get; set; }
        public string Region { get; set; }
    }

    public class Ec2Config
    {
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }
        public string DnsTagName { get; set; }
        public IEnumerable<string> Regions { get; set; }
    }
}