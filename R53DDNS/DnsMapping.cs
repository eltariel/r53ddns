namespace R53DDNS
{
    public class DnsMapping
    {
        public DnsMapping(string name, string ip)
        {
            Name = name;
            Ip = ip;
        }

        public string Name { get; }
        public string Ip { get; }
    }
}