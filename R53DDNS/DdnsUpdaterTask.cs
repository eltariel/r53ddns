using System.Threading.Tasks;
using NLog;

namespace R53DDNS
{
    class DdnsUpdaterTask : PeriodicTask
    {
        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        private readonly SystemConfig config;

        public DdnsUpdaterTask(SystemConfig config) : base("DDNS Updater", config.UpdateInterval)
        {
            this.config = config;
        }

        protected override async Task RunAsync()
        {
            log.Info("Starting update run...");

            var ec2 = new Ec2Accessor(config);
            var r53 = new Route53Accessor(config);

            var mappings = ec2.GetDnsMappings();
            r53.BindAddresses(mappings);

            log.Info("Done.");
        }
    }
}