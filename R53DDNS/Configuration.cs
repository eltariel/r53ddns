using System;
using System.Collections.Generic;
using System.IO;
using Amazon;
using Amazon.Runtime;
using Newtonsoft.Json;

namespace R53DDNS
{
    /// <summary>
    /// Configuration handling methods.
    /// </summary>
    public class Configuration
    {
        private static readonly SystemConfig defaultConfig = new SystemConfig
        {
            Ec2 = new Ec2Config
            {
                AccessKey = "<access key>",
                SecretKey = "<secret key>",
                Regions = new [] { "ap-southeast-2", "us-east-1", },
                DnsTagName = "R53DnsName",
            },
            Route53 = new Route53Config
            {
                AccessKey = "<access key>",
                SecretKey = "<secret key>",
                Region = "ap-southeast-2",
                DnsZoneName = "ft.eltariel.com.",
            },
            UpdateInterval = TimeSpan.FromHours(1),
        };

        /// <summary>
        /// Load a configuration from a JSON file.
        /// </summary>
        /// <param name="configPath">The path to the configuration file. Default path is config.json.</param>
        /// <returns>The configuration from the file, or a default config if the file does not exist.</returns>
        /// <remarks>If the file does not exist, a default configuration is written to the file and this is returned instead.</remarks>
        public static SystemConfig Load(string configPath = "config.json")
        {
            SystemConfig config;
            var js = JsonSerializer.Create(new JsonSerializerSettings { Formatting = Formatting.Indented, });

            if (File.Exists(configPath))
            {
                using (var sr = new StreamReader(configPath))
                using (var jr = new JsonTextReader(sr))
                {
                    config = js.Deserialize<SystemConfig>(jr);
                }
            }
            else
            {
                config = defaultConfig;

                using (var tw = new StreamWriter(configPath))
                using (var jtw = new JsonTextWriter(tw))
                {
                    js.Serialize(jtw, config);
                }
            }

            return config;
        }
    }
}