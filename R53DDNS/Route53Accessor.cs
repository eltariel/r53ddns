using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Amazon;
using Amazon.Route53;
using Amazon.Route53.Model;
using Amazon.Runtime;
using NLog;

namespace R53DDNS
{
    public class Route53Accessor
    {
        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        private readonly SystemConfig config;
        private readonly AWSCredentials credentials;
        private readonly RegionEndpoint region;

        public Route53Accessor(SystemConfig config)
        {
            this.config = config;
            this.credentials = new BasicAWSCredentials(config.Route53.AccessKey, config.Route53.SecretKey);
            this.region = RegionEndpoint.GetBySystemName(config.Route53.Region);
        }

        public void BindAddresses(List<DnsMapping> mappings)
        {
            var r53 = new AmazonRoute53Client(credentials, region);
            var zone = GetZone(config.Route53.DnsZoneName, r53);
            if (zone != null)
            {
                var rrSets = GetAResourceSetsInZone(zone, r53);

                var batch = new ChangeBatch();
                foreach (var mapping in mappings)
                {
                    var name = $"{mapping.Name}.{config.Route53.DnsZoneName}";
                    var rrSet = rrSets.FirstOrDefault(rr => rr.Name == name);

                    var change = rrSet != null
                        ? MakeUpdateRecordChange(mapping, rrSet)
                        : MakeCreateRecordChange(mapping, name);

                    batch.Changes.Add(change);
                }

                var br = r53.ChangeResourceRecordSets(new ChangeResourceRecordSetsRequest
                {
                    HostedZoneId = zone.Id,
                    ChangeBatch = batch
                });

                MonitorChangeRequest(br, r53);
            }
        }

        private static void MonitorChangeRequest(ChangeResourceRecordSetsResponse br, IAmazonRoute53 r53)
        {
            var cr = new GetChangeRequest {Id = br.ChangeInfo.Id};
            var cs = ChangeStatus.PENDING;
            while ((cs = r53.GetChange(cr).ChangeInfo.Status) == ChangeStatus.PENDING)
            {
                log.Debug("Change is pending.");
                Thread.Sleep(15000);
            }

            log.Debug($"Done, change result is {cs}");
        }

        private static HostedZone GetZone(string dnsZoneName, IAmazonRoute53 r53)
        {
            if (!dnsZoneName.EndsWith("."))
            {
                dnsZoneName = dnsZoneName + ".";
            }

            var zoneResponse = r53.ListHostedZones();
            var zone = zoneResponse.HostedZones.FirstOrDefault(z => z.Name == dnsZoneName);
            return zone;
        }

        private static List<ResourceRecordSet> GetAResourceSetsInZone(HostedZone zone, IAmazonRoute53 r53)
        {
            log.Debug($"Zone: {zone.Name}");
            var listRrRequest = new ListResourceRecordSetsRequest(zone.Id);
            var listRrResponse = r53.ListResourceRecordSets(listRrRequest);
            var rrSets = listRrResponse.ResourceRecordSets.Where(rr => rr.Type == RRType.A).ToList();
            return rrSets;
        }

        private static Change MakeUpdateRecordChange(DnsMapping mapping, ResourceRecordSet rrSet)
        {
            log.Debug($"Existing record: {rrSet.Name}. Set IP to {mapping.Ip}");
            rrSet.ResourceRecords[0].Value = mapping.Ip;
            return new Change(ChangeAction.UPSERT, rrSet);
        }

        private static Change MakeCreateRecordChange(DnsMapping mapping, string name)
        {
            log.Debug($"No record matching {name}, create new record.");
            return new Change(ChangeAction.CREATE, new ResourceRecordSet
            {
                Name = name,
                TTL = 60,
                Type = RRType.A,
                ResourceRecords = new List<ResourceRecord>
                {
                    new ResourceRecord {Value = mapping.Ip}
                }
            });
        }
    }
}