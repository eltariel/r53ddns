using System;
using System.Threading;
using System.Threading.Tasks;
using NLog;

namespace R53DDNS
{
    public abstract class PeriodicTask
    {
        private static readonly Logger log = LogManager.GetCurrentClassLogger();

        private readonly string taskName;
        private readonly TimeSpan period;
        private readonly Timer timer;

        private bool stopped = true;

        protected PeriodicTask(string taskName, TimeSpan period)
        {
            this.taskName = taskName;
            this.period = period;
            timer = new Timer(Callback, null, Timeout.Infinite, Timeout.Infinite);
        }

        public void Start()
        {
            log.Info("Starting task {0}", taskName);
            stopped = false;
            timer.Change(TimeSpan.Zero, period);
        }

        public void Stop()
        {
            log.Info("Stopping task {0}", taskName);
            stopped = true;
            timer.Change(Timeout.Infinite, Timeout.Infinite);
        }

        protected abstract Task RunAsync();

        private void Callback(object o)
        {
            RunAsync().Wait();
            if (!stopped)
            {
                timer.Change(period, Timeout.InfiniteTimeSpan);
            }
        }
    }
}