﻿using System;
using NLog;
using Topshelf;

namespace R53DDNS
{
    public class Program
    {
        private static readonly Logger log = LogManager.GetCurrentClassLogger();
        private DdnsUpdaterTask task;

        public Program(SystemConfig systemConfig)
        {
            log.Info("Constructing DDNS updater");
            task = new DdnsUpdaterTask(systemConfig);
        }

        static int Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += (sender, eventArgs) =>
            {
                log.Warn((Exception)eventArgs.ExceptionObject, "Unhandled Exception");
            };

            HostFactory.Run(x =>
            {
                x.Service<Program>(s =>
                {
                    s.ConstructUsing(name => MakeHost());
                    s.WhenStarted(h => h.Start());
                    s.WhenStopped(h => h.Stop());
                });

                x.SetDisplayName("Tank Monitor Client");
                x.SetServiceName("TankMonitor");

                x.StartAutomatically();
                x.RunAsNetworkService();

                x.UseNLog();
            });

            return 0;
        }

        private static Program MakeHost()
        {
            try
            {
                var config = Configuration.Load();
                return new Program(config);
            }
            catch (Exception ex)
            {
                log.Fatal(ex, "Exception creating service host");
                throw;
            }
        }

        private void Start()
        {
            task.Start();
        }

        private void Stop()
        {
            task.Stop();
        }

    }
}
